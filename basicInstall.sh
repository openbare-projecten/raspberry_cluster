#!/bin/bash

echo 'Install Docker'
sudo curl -sSL get.docker.com | sh
sudo usermod -aG docker pi
  
echo 'Disable swap'
sudo dphys-swapfile swapoff && \
  sudo dphys-swapfile uninstall && \
  sudo update-rc.d dphys-swapfile remove

echo 'add cmdline' 
sudo cp /boot/cmdline.txt /boot/cmdline.txt.org
orig="$(head -n1 /boot/cmdline.txt) cgroup_enable=cpuset cgroup_enable=memory"
sudo echo $orig | sudo tee /boot/cmdline.txt

echo 'install kubernetes' 

sudo apt-get update 
sudo apt-get install -y apt-transport-https curl
sudo curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo echo deb http://apt.kubernetes.io/ kubernetes-xenial main | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubeadm
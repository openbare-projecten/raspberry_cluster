Voor de instalatie moet een raspbian screts worden geflashed

# Pre-install
daarna moeten de volgende dingen ingesteld worden:
- hostname
- ip
- timezone
- locals
- expande system

Wanneer dat gedaan is kan alles worden geupgrade op de pi
```
sudo apt-get update && sudo apt-get upgrade -y
```
Hierna kan het script basicInstall.sh uitgevoerd worden

Wanneer dit script is uitgevoerd moet de pi worden herstart. Doe dit op alle nodes!

# Masternode

Voer de volgende commando uit om het cluster te maken (bij de apiserver moet het ip van de master komen te staan. )

```
sudo kubeadm init --token-ttl=0 --apiserver-advertise-address=192.168.1.10 --pod-network-cidr=10.244.0.0/16
```

Wanneer de bovenstaande commando uitgevoerd is staat er op het scherm dat je de volgende commandos moet uitvoeren. Deze zorgen er voor dat kubectl beschikbaar is als je niet root bent

```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

```

Ook staat er op het scherm een commando waarmee de andere nodes toegevoegd kunnen worden aan het cluster. Kopieer deze commando zodat deze later op de andere nodes uitgevoerd kunnen worden. 
Deze commando ziet er ongeveer zo uit

```
kubeadm join 192.168.178.21:6443 --token 7dkzfg.gwzzzhbhd889ow6i --discovery-token-ca-cert-hash sha256:e216dc979c8e64ebfb81cff2f472552b78d61340022f96df416d754572b0ee08
```

Nou is het van belang om flannel te instaleren, met flannel kunnen de verschillende nodes met elkaar praten.

```
curl -sSL https://rawgit.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml | sed "s/amd64/arm/g" | kubectl create -f -
curl -sSL https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml | sed "s/amd64/arm/g" | kubectl create -f -

TODO kijken welke goed werkt
```

Wanneer je wilt dat er ook pods op de master node komen te draaien kan dat doormiddel van de volgende commando
```
kubectl taint nodes --all node-role.kubernetes.io/master-
```

# Loadbalancer
Dit doe je allemaal op de masternode
(https://docs.traefik.io/user-guide/kubernetes/)
Om een loadbalancer te gebruiken heb ik gekozen voor traefik om deze te instaleren, hiervoor zijn een aantal dingen die geinstaleerd moeten worden.
Ten eerste moet de rbac gemaakt worden. Dit kan doormiddel van eht volgende
```
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/traefik-rbac.yaml
```

Hierna hebben we een aantal configuratie die we willen instellen. Hiervoor hebben we op de netwerk schijf een bestand gemaakt namelijk 'traefik-config.toml'
In dit bestand staat een aantal configuratie dingen zoals het automatisch redirecten van http verkeer naar https en ook automatisch letsencrypt certificaten maken
```
debug = false

logLevel = "ERROR"
defaultEntryPoints = ["https","http"]

[entryPoints]
  [entryPoints.http]
  address = ":80"
    [entryPoints.http.redirect]
    entryPoint = "https"
  [entryPoints.https]
  address = ":443"
  [entryPoints.https.tls]

[retry]

[acme]
email = "rvankammen@live.nl"
storage = "/exthdd/encrypt/acme.json"
entryPoint = "https"
onHostRule = true
[acme.httpChallenge]
entryPoint = "http"
[[acme.domains]]
main = "robertvankammen.nl"
```

Hierna hebben we traefik verder gestart doormiddel van het volgende:

```
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: traefik-ingress-controller
  namespace: kube-system
---
kind: DaemonSet
apiVersion: extensions/v1beta1
metadata:
  name: traefik-ingress-controller
  namespace: kube-system
  labels:
    k8s-app: traefik-ingress-lb
spec:
  template:
    metadata:
      labels:
        k8s-app: traefik-ingress-lb
        name: traefik-ingress-lb
    spec:
      serviceAccountName: traefik-ingress-controller
      terminationGracePeriodSeconds: 60
      containers:
      - image: traefik
        name: traefik-ingress-lb
        ports:
        - name: http
          containerPort: 80
          hostPort: 80
        - name: https
          containerPort: 443
          hostPort: 443
        - name: admin
          containerPort: 8080
          hostPort: 8080
        securityContext:
          capabilities:
            drop:
            - ALL
            add:
            - NET_BIND_SERVICE
        volumeMounts:
        - mountPath: "/exthdd"
          name: exthdd-vol
        args:
        - --configfile=/exthdd/config/traefik-config.toml
        - --api
        - --kubernetes
      volumes:
      - name: exthdd-vol
        persistentVolumeClaim:
         claimName: nfs-config
---
kind: Service
apiVersion: v1
metadata:
  name: traefik-ingress-service
  namespace: kube-system
spec:
  selector:
    k8s-app: traefik-ingress-lb
  ports:
    - protocol: TCP
      port: 443
      name: web
    - protocol: TCP
      port: 8080
      name: admin
```

Hierna kan er ook nog een ui worden gestart waarbij alles zichtbaar is. 

```
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/master/examples/k8s/ui.yaml
```

Om traefik ook extern beschikbaar te hebben moet de volgende confiratie worden geruned
```
kind: Service
apiVersion: v1
metadata:
  name: traefik-ingress-service
  namespace: kube-system
spec:
  selector:
    k8s-app: traefik-ingress-lb
  ports:
    - protocol: TCP
      port: 80
      name: http
    - protocol: TCP
      port: 443
      name: https
  externalIPs:
    - 192.168.178.21 # This is the node address
```

# Netwerkschijf
Voor een netwerk schijf hebben we gekozen voor een nfs schijf. Dit omdat deze makkelijk te koppelen is aan kubernetes. 
Voor een instalatie handleiding voor een nfs schijf op een raspberry pi volg deze link: https://www.htpcguides.com/configure-nfs-server-and-nfs-client-raspberry-pi/

Wanneer er een nfs schijf beschikbaar is in het netwerk kan de volgende yaml gemaakt worden en uitgevoerd worden. Het volgende script is nodig voor de configuratie van traefik
```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: config-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteMany
  nfs:
    server: 192.168.178.20
    path: /media/usb-config/cluster
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: nfs-config
  namespace: kube-system
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 100Mi
```

Hierin moet de server en de path in gevuld worden van je nfs schijf

#Private repository
Om een image te pullen van een private repository moet er een secret worden gemaakt hiervoor zoals hier onder staat
```
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>
```